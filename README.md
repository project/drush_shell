# Drush PsySh Interactive Shell

[![GitLab CI][gitlab-ci-badge]][gitlab-ci]

Run interactive shell with Drupal fully bootstraped.
Full REPL (Read-Eval-Print-Loop) support.

A Drush re-implementation of the old [Drupal Console][drupal-console]'s
[shell][drupal-console-shell] command. Also powered by [PsySh][psysh].

[gitlab-ci]: https://git.drupalcode.org/project/drush_shell/-/pipelines?page=1&scope=all&ref=1.0.x
[gitlab-ci-badge]: https://git.drupalcode.org/project/drush_shell/badges/1.0.x/pipeline.svg?key_text=Branch%201.0.x&key_width=90
[drupal-console]: https://drupalconsole.com/
[drupal-console-shell]: https://drupalconsole.com/docs/en/commands/shell
[psysh]: https://psysh.org/


## Installation

Install the module:

```sh
composer require drupal/drush_shell
```

Then enable the module.


## Usage

```sh
drush psysh
```

or

```sh
drush shell
```

or

```sh
drush sh
```
