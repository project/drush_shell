<?php

namespace Drupal\drush_shell\Drush\Commands;

use Drush\Commands\DrushCommands;
use Psy\Configuration as PsyConfiguration;
use Psy\Shell as PsyShell;

/**
 * Drush PsySH Commands.
 *
 * A Drush Commands implementation to provide interactive
 * shell related commands.
 */
class PsyshCommands extends DrushCommands {

  /**
   * Open a PsySH shell.
   *
   * @command psysh
   * @aliases psy shell sh
   * @bootstrap full
   */
  public function psysh() {
    $this->output()->writeln('Opening PsySH shell...');
    $this->output()->writeln('Type `exit` to return to Drush.');
    $this->output()->writeln('');

    $config = new PsyConfiguration();
    $shell = new PsyShell($config);
    $shell->run();
  }

}
